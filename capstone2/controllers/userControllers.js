const User = require(`../models/userModel.js`);

const bcrypt = require(`bcrypt`);
const auth = require(`../auth.js`);
const { response } = require("express");
const Product = require(`../models/productModel.js`);



//controllers

//user registration
module.exports.registerUser = (request, response) => {
    User.findOne({email : request.body.email})
    .then(result =>{
        if(result){
            return response.send(false)
        }else{
            let newUser = new User({
                firstName : request.body.firstName,
                middleName : request.body.firstName,
                lastName : request.body.lastName,
                mobileNo : request.body.mobileNo,
                email : request.body.email,
                password : bcrypt.hashSync(request.body.password, 10),
                isAdmin : request.body.isAdmin
            })

            newUser.save()
            .then(saved => response.send(true))
            .catch(error => response.send(false))
        }
    }).catch(error => response.send(false))
}


//_____Gget users
module.exports.allUsers = async(request, response)=>{
    const userList = await User.find({})
    
        response.send(userList);
}







//____________create order
module.exports.createOrder = async (request, response) => {
    const productId = request.body.id;
    const userData = auth.decode(request.headers.authorization);
  
    if (userData.isAdmin) {
      return response.send(false);
    } else {
      try {
        const isUserUpdated = await User.findOneAndUpdate(
          { _id: userData.id },
          { $push: { orderedProduct: { productId: productId } } },
          { new: true }
        );
  
        const product = await Product.findOne({ _id: productId });
  
        if (product && isUserUpdated) {
          product.userOrders.push({ userId: userData.id });
          await product.save();
  
          return response.send(true);
        } else {
          return response.send(false);
        }
      } catch (error) {
        console.error("Error creating order:", error);
        return response.send(false);
      }
    }
  };
  


//___________________________login
module.exports.login = (request, response) => {

    User.findOne({email : request.body.email})
    .then(result =>{
        if(!result){
            return response.send(false)
        }else{
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            
            if(isPasswordCorrect){
                return response.send({
                    auth: auth.createAccessToken(result)
                })
            }else{
                return response.send(false)
            }
        }
    }).catch(error => response.send(false));
}






module.exports.userDetails = (request, response) => {

    User.findOne({email : request.body.email})
    .then(result =>{
        if(!result){
            return response.send(`The email, ${request.body.email} is not yet registered`)
        }else{
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            
            if(isPasswordCorrect){
                response.send(result)
            }else{
                return response.send(`Incorrect password!`)
            }
        }
    }).catch(error => response.send(error));
}




module.exports.getUserId = (request, response) =>{
    User.findById(request.params.id)
    .then(result=>{
        if(!result){
            response.send(`User not found`)
        }else{
            response.send(result)
        }
    }).catch(error => response.send(error))
}

module.exports.retrieveUserDetails = (request, response)=>{
    const userData = auth.decode(request.headers.authorization);

    User.findOne({_id: userData.id})
    .then(data => response.send(data));
}