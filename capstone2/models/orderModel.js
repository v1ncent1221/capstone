const mongoose = require(`mongoose`);

const orderSchema = mongoose.Schema({

    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: `User`
    },
    orderItems : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : "orderItem",
        required : true
    }],
    status : {
        type : String,
        required: true,
        default: `Pending`
    },
    dateOrdered: {
        type: Date,
        default: Date.now()
    }
    

})

exports.Order = mongoose.model('Order', orderSchema);