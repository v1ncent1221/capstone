const mongoose = require(`mongoose`);

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, `name is required`]
    },
    description : {
        type : String,
        required : [true, `Passowrd is requred`]
    },
    price : {
        type : Number,
        required : [ true, `Price is required`]
    },
    stock : {
        type: Number,
        required : [true, `stock count is requried`]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    userOrders : [{
        userId : {
            type : String,
            required : [true, `User ID is required`]
        }
    }]
    
});

const Product = mongoose.model(`product`, productSchema);
module.exports = Product; 