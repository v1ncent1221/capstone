const express = require(`express`);
const userControllers = require(`../controllers/userControllers.js`);
const router = express.Router();

const auth =require(`../auth.js`);

router.post(`/register`, userControllers.registerUser);

router.post(`/login`, userControllers.login);


router.get(`/userDetails`, userControllers.userDetails)
router.get(`/users`, userControllers.allUsers)
router.get(`/userProfile`, auth.verify, userControllers.retrieveUserDetails)


router.post(`/createOrder`, auth.verify, userControllers.createOrder)

router.get(`/:id`, userControllers.getUserId)


module.exports = router; 