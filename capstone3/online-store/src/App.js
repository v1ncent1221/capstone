import './App.css';
import { useEffect, useState } from 'react';
import Home from './pages/Home';
import AppNavBar from './components/AppNavBar';
import Login from './pages/Login';
import Products from './pages/Products';
import Logout from './pages/Logout';
import ProductView from './pages/ProductView';
import Page404 from './pages/Page404';
import AdminProduct from './components/AdminProducts';

import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
import Register from './pages/Register';
import AdminDashBoard from './pages/AdminDashBoard';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';



function App() {
  
  

  let userDetails = {};

  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null 
      }
      
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/user/userProfile`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
        console.log(data)
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])


  const [user, setUser] = useState(userDetails);

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
  },[user])

  return (
    <UserProvider value = {{user,setUser, unsetUser}}>
    <BrowserRouter>
      <AppNavBar />
      <Routes>
        <Route path = '/' element = {<Home />} />
        <Route path = '/login' element = {<Login />} />
        <Route path = '/register' element = {<Register />} />
        <Route path = '/products' element = {<Products />}/>
        <Route path = '/productId:/:productId' element = {<ProductView />} />
        <Route path = '/logout' element = {<Logout />} />
        <Route path = '/dashboard' element = {<AdminDashBoard />}/>
        <Route path = '/addProduct' element = {<CreateProduct />}/>
        <Route path = '*' element = {<Page404/>}/>
        <Route path = '/updateProduct:/:productId' element ={<UpdateProduct/>}/>
      </Routes>
    </BrowserRouter>
    </UserProvider>
  );
}

export default App;
