import { Container, Row, Col, Card, Button, Carousel } from "react-bootstrap"
import img1 from "../images/10611.jpg"
import img2 from "../images/10612.jpg"
import img3 from "../images/10613.jpg"
import { useState, useEffect } from "react"
import FeaturedProduct from "./FeaturedProduct"
import { Link } from "react-router-dom"



export default function HomeComponents(){
  const [product, setProduct] = useState([])


  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/product/activeProducts`)
    .then(result => result.json())
    .then(data =>{
        setProduct(data.map(product=>{
            return(
                <FeaturedProduct key = {product._id} productProp = {product}/>
            )
        }))
    })
},[])
    return(
        <>
        <div className="mt-5 pt-3 ">
          <Carousel>
            <Carousel.Item>
              <img className="d-block w-100" src={img1} alt="First slide"/>

            </Carousel.Item>
            <Carousel.Item>
              <img className="d-block w-100" src={img2}alt="Second slide"/>

            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={img3} alt="Third slide" />

            </Carousel.Item>
          </Carousel>
        </div>
        <div className="text-light">
          <Container>
            <h1 className="text-center">Tinker Gaming Shop</h1>
            <Row >
                <h2 className="text-center">Featured Products</h2>
                {product}
                
              </Row>
              <Row className="text-center">
                <Col className="col-lg-6 col-sm-12">
                <h2 className="text-center">Contact</h2>
                <h6>+63 9534 584 1231</h6>
                <h6>+63 9478 934 1473</h6>
                </Col>
                <Col className="col-lg-6 col-sm-12 ">
                <h2 className="text-center">Socials</h2>
                <h6><Link to = '/register' style={{ textDecoration: 'none' }}>Facebook</Link></h6>
                <h6><Link to = '/register' style={{ textDecoration: 'none' }}>Youtube</Link></h6>
                <h6><Link to = '/register' style={{ textDecoration: 'none' }}>Twitter</Link></h6>
                </Col>
              </Row>
              <Row className="text-center col-lg-8 mx-auto">
                <Col className="text-center">
                <h2>About us</h2>
                <p>Tinker was founded by a group of friends who share the same interests when it comes to hobby. It all started in 2015 when one of them moved to japan for a job and live for three years. Upon going home, he was requested of his friends to buy numbers of things like Anime merchandise, latest PC components that was not available to their country, and many more. The things he had to buy was so beyond him that it struck him the idea to start this business with them to ship and resell popular and demanded products in the world of gaming and anime. Now our company offers the same service and hold to the standard of selling the latest and the hotest products that are demanded by our buyers!</p>
                </Col>
              </Row>
          </Container>
        </div>
        </>
    )
}