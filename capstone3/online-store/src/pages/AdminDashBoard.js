import { Container, Row, Col, Button, Table } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import ProductsCard from "../components/ProductsCard";
import { Link, Navigate } from "react-router-dom";
import AdminProduct from "../components/AdminProducts";
import UserContext from "../UserContext";



export default function AdminDashBoard(){


    const [product, setProduct] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/product/allProduct`,{
            method : 'GET',
            headers : {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data =>{
            console.log(data)
            setProduct(data.map(product=>{
                return(
                    <AdminProduct key = {product._id} productProp = {product}/>
                )
            }))
        })
    },[])
    const {user} = useContext(UserContext)

    return (
        (user.id !== null) ?
        <Container className="mt-5 pt-4">
            <h2 className="text-center text-light">Admin Dashboard</h2>
            <Row>
                <Col className="text-center pb-3">
                    <Button  as = {Link} to ='/addProduct'>Create Product</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>Product Name</th>
                            <th>Product Description</th>
                            <th>Product Price</th>
                            <th>Stock left</th>
                            <th>Available</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {product}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
        :
        <Navigate to = '*'/>
    )
}