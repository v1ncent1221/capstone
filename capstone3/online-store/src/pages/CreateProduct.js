import { useEffect, useState, useContext } from "react";
import { Col, Container, Row, Form, Button, Card } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal2 from "sweetalert2";
import UserContext from "../UserContext";


export default function CreateProduct(){
    
    const [name, setName] = useState('');
    const [description, setDescription] = useState ('');
    const [price, setPrice] = useState ('');
    const [stock, setStock] = useState ('');
    const [isActive, setIsActive] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const navigate = useNavigate();

    useEffect(()=>{
        let fieldReqs = (name !== "" && description !== "" && price !== "" && stock !== "")
        if(fieldReqs){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[name, description, price, stock])

    function createProduct(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/product/addProduct`,{
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body : JSON.stringify({
                name:name,
                description:description,
                price:price,
                stock:stock
            })
        })
        .then(result => result.json())
        .then(data =>{
            console.log(data)
            if(data === false){
                Swal2.fire({
                    "title":"Creation failed",
                    icon: "error",
                    text: "Product creation failed! Please try again"
                })
            }else{
                Swal2.fire({
                    "title":"Product Created",
                    icon: "success",
                    text: "Product successfully created"
                })
                
            }
            navigate('/dashboard')
        })
    }
    const {user} = useContext(UserContext)

    return(
        (user.id !== null) ?
        <Container className="mt-5 pt-3">
            <Row>
                <h2 className="text-center text-light">Product Creation</h2>
                <p className="text-center text-light">Please fill the following fields</p>
                <Col className="col-lg-8 mx-auto">
                    <Form onSubmit = {event=> createProduct(event)}>
                        <Card>
                            <Card.Body>
                            <Form.Group className="mb-2" controlId="formName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Product Name" value={name} onChange={event =>{
                                setName(event.target.value)
                            }}/>
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="formDescription">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control type="text" placeholder="Enter Product Description" value={description} onChange={event =>{
                                setDescription(event.target.value)
                            }}/>
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="formPrice">
                            <Form.Label>Product Price</Form.Label>
                            <Form.Control type="Number" placeholder="Enter Product Price" value={price} onChange={event =>{
                                setPrice(event.target.value)
                            }}/>
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="formStock">
                            <Form.Label>Stock Count</Form.Label>
                            <Form.Control type="Number" placeholder="Enter Product Count" value={stock} onChange={event =>{
                                setStock(event.target.value)
                            }}/>
                        </Form.Group>
                            </Card.Body>


                            <Card.Footer>
                            <small className="d-grid gap-2">
                                <Button  className="mt-2 rounded-0" variant="primary" type="submit" disabled = {isDisabled} >
                            Submit
                            </Button>
                            
                            </small>
                            
                            </Card.Footer>
                        </Card>

                        

                        
                    </Form>
                </Col>
            </Row>
        </Container>
        :
        <Navigate to = '*'/>
    )
}