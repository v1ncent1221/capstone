import { Container, Row, Col, Form, Button, Card } from "react-bootstrap"
import { useState, useEffect, useContext } from "react"
import { Navigate, Link, useNavigate  } from "react-router-dom";

import UserContext from "../UserContext";
import Swal2 from "sweetalert2";

export default function Login(){
    const [isDisabled, setIsDisabled] = useState(true)
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const {user, setUser} = useContext(UserContext);

    const retrieveUserDetails = (token) =>{
        fetch(`http://localhost:4000/user/userProfile`,{
            method : 'GET',
            headers : {
                Authorization : `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data=>{
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    const navigate = useNavigate();

    useEffect(()=>{
        let notBlank = (email !=='' && password !=='')
        if(notBlank){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[email, password]);

    const login = (event) =>{
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
            method: 'POST',
            headers:{
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password: password
            })
        })
        .then(result => result.json())
        .then(data =>{
            if(data === false){
                Swal2.fire({
                    "title":"login failed",
                    icon: "error",
                    text: "Check log in details and try again"

            })
            }else{
                localStorage.setItem('token', data.auth);
                retrieveUserDetails(data.auth);

                Swal2.fire({
                    "title":"login successful",
                    icon: "success",
                    text: "Welcome to Zuitt"

            })
                navigate('/')
            }
        })
    }



    return user.id === true ?(

       <Navigate to = '*'/>
    ):(
        <Container className="mt-5 pt-4">
            <Row>
                <h1 className="text-center text-light">Login</h1>
                <Col className="col-lg-6 mx-auto">
                    <Form onSubmit={event=> login(event)}>
                        <Card className="rounded-0">
                            <Card.Body>
                                <Form.Group className="mb-2" controlId="formBasicEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={event =>{
                                        setEmail(event.target.value)
                                    }}/>
                                </Form.Group>
                        
                                <Form.Group className="mb-2" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" value={password} onChange={event =>{
                                        setPassword(event.target.value)
                                    }}/>
                                </Form.Group>
                                <p>Dont have an account yet? Sign up <Link to = '/register'>here</Link></p>

                                
                            </Card.Body>
                            <Card.Footer>
                            <small className="d-grid gap-2">
                                    <Button variant="primary" type="submit" disabled={isDisabled} className="rounded-0">
                                    Submit
                                    </Button>
                                </small>
                            </Card.Footer>
                            </Card>      
                        </Form>    
                </Col>
            </Row>
        </Container>
    
    )
   
}