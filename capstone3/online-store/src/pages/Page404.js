import { Container, Row, Col,  } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Page404(){
    return(
        <Container className="mt-5 pt-3">
            <Row>
                <Col className="text-center ">
                <h2 className="text-light">Page Not Found</h2>
                <p className="text-light">Go to back to the <Link to = '/'>homepage</Link></p>
                
                </Col>
            </Row>
        </Container>
    )
}