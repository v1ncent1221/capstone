import { Container, Row, Col, Card, Button } from "react-bootstrap";
import ProductsCard from "../components/ProductsCard";
import { useEffect, useState } from "react";
    

export default function Products(){
    const [product, setProduct] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/product/activeProducts`)
        .then(result => result.json())
        .then(data =>{
            setProduct(data.map(product=>{
                return(
                    <ProductsCard key = {product._id} productProp = {product}/>
                )
            }))
        })
    },[])

    return(
        <>
            <h1 className="text-center pt-4 mt-5 text-light">Products</h1>
            <Container>
                <Row>
                    
                        {product}
                    
                </Row>
            </Container>
        </>
    )
}