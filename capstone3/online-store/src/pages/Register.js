import { Col, Container, Row, Form, Button, Card } from "react-bootstrap";
import { useState, useEffect, useContext } from "react"
import { Link, useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal2 from "sweetalert2";

export default function Register(){
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState ('');
    const [password2, setPassword2] = useState ('');
    const [firstName, setFirstName] = useState ('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const navigate = useNavigate();
    const [isDisabled, setIsDisabled] = useState(true);
    const [middleName, setMiddlename] = useState ('');

    useEffect(()=>{
        let fieldReqs = (email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && password1.length > 6 && firstName !== '' && lastName !== '' && mobileNo.length >= 11)
        if(fieldReqs){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[email, password1, password2, firstName, lastName, mobileNo]);

    function register  (event){
        event.preventDefault()
        let password = password1

        fetch(`${process.env.REACT_APP_API_URL}/user/register`,{
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                firstName:firstName,
                lastName:lastName,  
                email:email,
                password: password,
                mobileNo:mobileNo

            })
        })
        .then(result => result.json())
        .then(data =>{
            if(data === false){
                Swal2.fire({
                    "title":"registration failed",
                    icon: "error",
                    text: "there was an error in the registration. please try again"
                })
            }else{
                Swal2.fire({
                    "title":"registration successful",
                    icon: "success",
                    text: "proceed to log in"
                })
                navigate('/')
            }
        })
    }
    const {user, setUser} = useContext(UserContext);




    return user.id === null ?(
       
        <Container className="mt-5 pt-4 pb-3">
        <Row>
            <h1 className="text-center text-light">Register</h1>
            <Col className="col-lg-6 mx-auto">
            <Form onSubmit ={event=> register(event)}>
                <Card className="rounded-0">
                    <Card.Body>
                          
                            <Form.Group className="mb-2" controlId="formFirstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={event =>{
                                    setFirstName(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="formMiddleName">
                                <Form.Label>Middle Name(Optional)</Form.Label>
                                <Form.Control type="text" placeholder="Enter Middle Name" value={middleName} onChange={event =>{
                                    setMiddlename(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="FormLastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={event =>{
                                    setLastName(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" value={email} onChange={event =>{
                                    setEmail(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="formBasicPassword1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" value={password1} onChange={event =>{
                                    setPassword1(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="formBasicPassword2">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control type="password" placeholder="Retype Password" value={password2} onChange={event =>{
                                    setPassword2(event.target.value)
                                }}/>
                            </Form.Group>

                            <Form.Group className="mb-2" controlId="formMobileNo">
                                <Form.Label>Contact Number</Form.Label>
                                <Form.Control type="text" placeholder="Enter Contact Number" value={mobileNo} onChange={event =>{
                                    setMobileNo(event.target.value)
                                }}/>
                            </Form.Group>

                            <p>Already have an account? Sign in <Link to = '/login'>here</Link></p>
                            
                    </Card.Body>
                    <Card.Footer>
                        <small className="d-grid gap-2">
                            <Button variant="primary" type="submit" disabled = {isDisabled} className="rounded-0" >
                                Submit
                            </Button>
                        </small>
                    </Card.Footer>
                </Card>
            </Form>
                
            </Col>
        </Row>
    </Container>
        
    ):(
       
        <Navigate to = '*'/>
    )
}